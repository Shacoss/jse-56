package ru.koptev.jse.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.koptev.jse.dto.MessageDTO;
import ru.koptev.jse.model.Message;

@Mapper
public interface MessageMapper {

    MessageMapper INSTANT = Mappers.getMapper(MessageMapper.class);

    @Mapping(source = "user.login", target = "login")
    MessageDTO messageToMessageDTO(Message message);

}
