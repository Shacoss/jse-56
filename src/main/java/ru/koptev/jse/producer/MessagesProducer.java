package ru.koptev.jse.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.koptev.jse.dto.mapper.MessageMapper;
import ru.koptev.jse.model.Message;

@Component
public class MessagesProducer {

    private final RabbitTemplate rabbitTemplate;

    private final MessageMapper messageMapper;

    @Autowired
    public MessagesProducer(RabbitTemplate rabbitTemplate, MessageMapper messageMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.messageMapper = messageMapper;
    }

    public void messagesProduced(Message message) {
        rabbitTemplate.convertAndSend(message.getChat().getName(), "", messageMapper.messageToMessageDTO(message));
    }

}
