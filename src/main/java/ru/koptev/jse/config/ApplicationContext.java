package ru.koptev.jse.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.koptev.jse.dto.mapper.MessageMapper;
import ru.koptev.jse.model.InputParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@Configuration
public class ApplicationContext {

    public static final int CHAT_INDEX = 0;
    public static final int USERNAME_INDEX = 1;
    private static final int PASSWORD_INDEX = 2;
    private static final int ARGUMENT_COUNT = 3;
    private final ApplicationArguments applicationArguments;

    public ApplicationContext(ApplicationArguments applicationArguments) {
        this.applicationArguments = applicationArguments;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public <T> List<T> arrayList() {
        return new ArrayList<>();
    }

    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    @Bean
    public MessageMapper messageMapper() {
        return MessageMapper.INSTANT;
    }

    @Bean
    public InputParam inputParam() {
        if (applicationArguments.getSourceArgs().length == ARGUMENT_COUNT) {
            return InputParam.builder()
                    .chat(applicationArguments.getSourceArgs()[CHAT_INDEX])
                    .login(applicationArguments.getSourceArgs()[USERNAME_INDEX])
                    .password(applicationArguments.getSourceArgs()[PASSWORD_INDEX])
                    .build();
        } else {
            return null;
        }
    }

}
