package ru.koptev.jse.util;

import ru.koptev.jse.enumerated.Operation;
import ru.koptev.jse.model.QueryParam;

import java.util.List;

public class QueryStringBuilderUtil {

    private static final int LAST_COUNT_SYMBOLS = 4;

    private QueryStringBuilderUtil() {
    }

    public static String queryString(List<QueryParam> queryParams) {
        StringBuilder stringBuilder = new StringBuilder();
        for (QueryParam queryParam : queryParams) {
            if (queryParam.getOperation().equals(Operation.BETWEEN)) {
                stringBuilder
                        .append(queryParam.getColumnName()).append(" ")
                        .append(queryParam.getOperation().getValue()).append(" ")
                        .append("'").append(queryParam.getBetweenStarDate()).append("'")
                        .append(" ").append("AND").append(" ")
                        .append("'").append(queryParam.getBetweenEndDate()).append("'")
                        .append(" ").append("AND").append(" ");
            } else {
                stringBuilder
                        .append(queryParam.getColumnName())
                        .append(queryParam.getOperation().getValue())
                        .append("'").append(queryParam.getColumnValue()).append("'")
                        .append(" ").append("AND").append(" ");
            }
        }
        stringBuilder.setLength(stringBuilder.length() - LAST_COUNT_SYMBOLS);
        return stringBuilder.toString();
    }

}
