package ru.koptev.jse.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.koptev.jse.enumerated.Operation;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QueryParam {

    private String columnName;
    private String columnValue;
    private Operation operation;
    private String betweenStarDate;
    private String betweenEndDate;

}
