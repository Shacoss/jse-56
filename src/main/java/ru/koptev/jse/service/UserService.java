package ru.koptev.jse.service;

import ru.koptev.jse.model.User;

import java.util.Optional;

public interface UserService {

    Optional<User> authorization(String login, String password);

    User create(String login, String password);

}
