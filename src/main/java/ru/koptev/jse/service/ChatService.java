package ru.koptev.jse.service;

import ru.koptev.jse.model.Chat;
import ru.koptev.jse.model.User;

import java.util.List;

public interface ChatService {

    Chat getChatByName(String name, User user);

    Chat create(String name, User user);

    List<Chat> findAll();
}
