package ru.koptev.jse.service;

import ru.koptev.jse.model.Chat;
import ru.koptev.jse.model.User;

public interface MessageService {

    void addMessage(String messageContent, Chat chat, User user);

}
