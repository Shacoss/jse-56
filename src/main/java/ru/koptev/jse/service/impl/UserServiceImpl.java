package ru.koptev.jse.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.koptev.jse.enumerated.Operation;
import ru.koptev.jse.model.Chat;
import ru.koptev.jse.model.QueryParam;
import ru.koptev.jse.model.User;
import ru.koptev.jse.repository.CommonRepository;
import ru.koptev.jse.service.UserService;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class UserServiceImpl implements UserService {

    private CommonRepository commonRepository;
    private List<QueryParam> queryParams;
    @Qualifier("chatList")
    private List<Chat> chats;

    @Lookup
    public List<Chat> getChats() {
        return null;
    }

    @Lookup
    public List<QueryParam> getQueryParams() {
        return null;
    }

    @Autowired
    public void setCommonRepository(CommonRepository commonRepository) {
        this.commonRepository = commonRepository;
    }

    @Override
    public Optional<User> authorization(String login, String password) {
        if (login == null || login.isEmpty()) {
            log.info("Empty login");
            return Optional.empty();
        }
        if (password == null || password.isEmpty()) {
            log.info("Empty password");
            return Optional.empty();
        }
        queryParams = getQueryParams();
        queryParams.add(QueryParam.builder()
                .columnName("login")
                .operation(Operation.EQUAL)
                .columnValue(login)
                .build());
        try {
            User user = commonRepository.getByColumn(User.class, queryParams).get(0);
            if (user.getPassword().equals(password)) {
                log.info("Authorization successfully");
                return Optional.of(user);
            }
            log.info("Wrong password");
            return Optional.empty();
        } catch (NoResultException | IndexOutOfBoundsException e) {
            log.info("User not found. Creating new user with login {}", login);
            return Optional.of(create(login, password));
        }
    }

    @Override
    public User create(String login, String password) {
        User user = User.builder().login(login).password(password).chats(getChats()).build();
        log.info("New user created successfully with login {}", login);
        return commonRepository.save(user);
    }

}
