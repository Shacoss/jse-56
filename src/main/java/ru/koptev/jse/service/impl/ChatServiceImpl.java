package ru.koptev.jse.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import ru.koptev.jse.enumerated.Operation;
import ru.koptev.jse.model.Chat;
import ru.koptev.jse.model.QueryParam;
import ru.koptev.jse.model.User;
import ru.koptev.jse.repository.CommonRepository;
import ru.koptev.jse.service.ChatService;

import javax.persistence.NoResultException;
import java.util.List;

@Log4j2
@Service
public class ChatServiceImpl implements ChatService {

    private CommonRepository commonRepository;
    private List<QueryParam> queryParams;
    private List<User> users;

    @Lookup
    public List<User> getUsers() {
        return null;
    }

    @Lookup
    public List<QueryParam> getQueryParams() {
        return null;
    }

    @Autowired
    public void setCommonRepository(CommonRepository commonRepository) {
        this.commonRepository = commonRepository;
    }


    @Override
    public Chat getChatByName(String name, User user) {
        queryParams = getQueryParams();
        queryParams.add(QueryParam.builder()
                .columnName("chat_name")
                .operation(Operation.EQUAL)
                .columnValue(name)
                .build());
        try {
            Chat chat = commonRepository.getByColumn(Chat.class, queryParams).get(0);
            int findUser = (int) chat.getUsers().stream()
                    .filter(u -> user.getUserId().equals(u.getUserId()))
                    .count();
            if (findUser == 0) {
                chat.addUser(user);
                commonRepository.update(chat);
            }
            return chat;
        } catch (NoResultException | IndexOutOfBoundsException e) {
            log.info("Chat not found. Creating new chat with name {}", name);
            return create(name, user);
        }
    }

    @Override
    public Chat create(String name, User user) {
        Chat chat = Chat.builder().name(name).users(getUsers()).build();
        chat.addUser(user);
        log.info("New chat created successfully with name {}", name);
        return commonRepository.save(chat);
    }

    @Override
    public synchronized List<Chat> findAll() {
        return commonRepository.getAll(Chat.class);
    }

}
