package ru.koptev.jse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.koptev.jse.model.Chat;
import ru.koptev.jse.model.Message;
import ru.koptev.jse.model.User;
import ru.koptev.jse.producer.MessagesProducer;
import ru.koptev.jse.repository.CommonRepository;
import ru.koptev.jse.service.MessageService;

import java.time.Instant;

@Service
public class MessageServiceImpl implements MessageService {

    private final CommonRepository commonRepository;

    private final MessagesProducer messagesProducer;

    @Autowired
    public MessageServiceImpl(CommonRepository commonRepository, MessagesProducer messagesProducer) {
        this.commonRepository = commonRepository;
        this.messagesProducer = messagesProducer;
    }

    @Override
    public void addMessage(String messageContent, Chat chat, User user) {
        Message message = Message.builder()
                .messageContent(messageContent)
                .chat(chat)
                .timestamp(Instant.now())
                .user(user)
                .build();
        commonRepository.save(message);
        messagesProducer.messagesProduced(message);
    }

}
