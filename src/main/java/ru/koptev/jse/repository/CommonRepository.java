package ru.koptev.jse.repository;

import ru.koptev.jse.model.QueryParam;

import java.util.List;

public interface CommonRepository {

    <T> T save(T entity);

    <T> void update(T entity);

    <T> T getById(Class<T> tClass, Long id);

    <T> List<T> getAll(Class<T> tClass);

    <T> List<T> getByColumn(Class<T> tClass, List<QueryParam> queryParams);

}
