package ru.koptev.jse.repository.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.koptev.jse.model.QueryParam;
import ru.koptev.jse.repository.CommonRepository;
import ru.koptev.jse.util.QueryStringBuilderUtil;

import java.util.List;


@Repository
public class CommonRepositoryImpl implements CommonRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <T> T save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(entity);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> void update(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public <T> T getById(Class<T> tClass, Long id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        T entity = session.get(tClass, id);
        transaction.commit();
        session.close();
        return entity;
    }

    @Override
    public <T> List<T> getAll(Class<T> tClass) {
        Session session = sessionFactory.openSession();
        List<T> entities = session.createQuery("SELECT e FROM " + tClass.getSimpleName() + " e", tClass).getResultList();
        session.close();
        return entities;
    }

    @Override
    public synchronized <T> List<T> getByColumn(Class<T> tClass, List<QueryParam> queryParams) {
        Session session = sessionFactory.openSession();
        List<T> entities = session.createQuery("SELECT e FROM " + tClass.getSimpleName() +
                " e WHERE " + QueryStringBuilderUtil.queryString(queryParams), tClass)
                .getResultList();
        session.close();
        return entities;
    }

}
