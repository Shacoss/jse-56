package ru.koptev.jse.process;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.koptev.jse.dto.MessageDTO;
import ru.koptev.jse.model.Chat;
import ru.koptev.jse.model.InputParam;
import ru.koptev.jse.model.User;
import ru.koptev.jse.service.ChatService;
import ru.koptev.jse.service.MessageService;
import ru.koptev.jse.service.UserService;

import java.util.Optional;
import java.util.Scanner;

@Log4j2
@Component
public class ApplicationProcess {

    private final InputParam inputParam;
    private final UserService userService;
    private final ChatService chatService;
    private final MessageService messageService;
    private final Scanner scanner;
    private final RabbitListenerEndpointRegistry container;
    private User user;
    private Chat chat;

    @Autowired
    public ApplicationProcess(InputParam inputParam, UserService userService, ChatService chatService,
                              MessageService messageService, Scanner scanner, RabbitListenerEndpointRegistry container) {
        this.inputParam = inputParam;
        this.userService = userService;
        this.chatService = chatService;
        this.messageService = messageService;
        this.scanner = scanner;
        this.container = container;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void process() {
        log.info("Chat started");
        authorization();
        if (user != null) {
            chat = chatService.getChatByName(inputParam.getChat(), user);
            sendMessage();
        }
    }

    private void authorization() {
        if (inputParam == null) {
            log.info("Authorization failed");
            return;
        }
        Optional<User> authorization = userService.authorization(inputParam.getLogin(), inputParam.getPassword());
        if (authorization.isPresent()) {
            user = authorization.get();
        } else {
            log.info("Authorization failed");
        }
    }

    private void sendMessage() {
        log.info("Enter to chat {}", chat.getName());
        container.start();
        String messageContent;
        while (true) {
            messageContent = scanner.nextLine();
            if ("--close_chat".equals(messageContent)) {
                break;
            }
            messageService.addMessage(messageContent, chat, user);
        }
    }

    public void receivedMessage(MessageDTO message) {
        System.out.printf("%s - %s: %s %n",
                message.getTimestamp(),
                message.getLogin(),
                message.getMessageContent());
    }

    public User getUser() {
        return user;
    }
}
