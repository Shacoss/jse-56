package ru.koptev.jse.enumerated;

public enum Operation {

    EQUAL("="),
    NOT_EQUAL("!="),
    MORE(">"),
    LESS("<"),
    BETWEEN("BETWEEN");

    private final String value;

    Operation(String operation) {
        this.value = operation;
    }

    public String getValue() {
        return value;
    }

}
