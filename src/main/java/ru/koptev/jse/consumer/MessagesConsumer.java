package ru.koptev.jse.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.koptev.jse.dto.MessageDTO;
import ru.koptev.jse.process.ApplicationProcess;

@Component
@RabbitListener
public class MessagesConsumer {

    private final ApplicationProcess applicationProcess;

    @Autowired
    public MessagesConsumer(ApplicationProcess applicationProcess) {
        this.applicationProcess = applicationProcess;
    }

    @RabbitListener(queues = "#{inputParam.login}", autoStartup = "false")
    public void receivedMessage(MessageDTO messageDTO) {
        if (applicationProcess.getUser().getLogin().equals(messageDTO.getLogin())) {
            return;
        }
        applicationProcess.receivedMessage(messageDTO);
    }

}
